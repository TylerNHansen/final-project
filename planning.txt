First feature: start with sessions. User should get a session cookie
when visiting the index page. Get it to display an identifier at the top.

Get the DB to persist the no-name/password sessions. Test this by opening
up an incognito window - should make a new anonymous account.

There's a design decision here - keep anon account info inside a cookie, or
persist it to a database. I think I need to push it all to the client-side
until they want it persisted under a name/password combination. I mean,
way of accessing it other than from the cookie, there's no goddamn point
to saving it server-side. Wait, never mind, we want anon accounts to have
full functionality, which means upvotes/downvotes/views, which means
persisting who likes what to the DB. Save that shit - it's useful.

So, the design choice is figuring out where and when to trim un-needed data.
Maybe something like no votes on recently viewed content and no recent access.

--

The identifier is just some function of the user ID. Some way of mapping
from number to strings - not sure if I want a series of words
(think 'correct horse battery staple'). Scratch that, definitely want
a series of words like that.


Next, make sign-up work. It's basically just adding an email and password
to a user account. Or some way of identifying yourself - it actually isn't
really that important at this point. Basically anything that you can set
as something only you can use to get to your account.


--------------------------

Model - class User, with indexed token, email, password_digest (for now).
DB does not require any of these. This is fine - if they don't have it,
users are only unable to reach the accounts. I still want them for keeping
track of what posts people like.

Router - just the root page for now, make the site-wide stuff work first

Controller - going to have just one path. Need an application controller
for site-wide auth stuff and sessions. Index only for now.

Views - index.html. Just a success message, later features will fill this in.

-------

Got automagic sign-up working. Can't set email/password yet, should
that be next? It's that or adding links to display. Got side-tracked on
mapping session tokens to human readable words. Will come back later for
it at a later stage.

--------

Next up, posts. I might want to constrain this down to "images or text".
Maybe want links. Links will make things funky for any specialized
load-content-of-one-post-and-show-it UI. Actually, I think I want links.

Probably want to classify these into text, image, and link posts.
Scratch that. I want to add a list (hash?) of classifications, and parse from the
submission some default classifications. The general idea is being able
to browse all images, or all cat images, or all cat, or all text posts about relationships

So the basic idea for links is full CRUD. Attributes:
  target - optional string. URL for the content in question. Self posts have
    a null target - asking for the target will default back to the location
  location - uniq string. URL for the post itself.
    actually, don't store in database, because that's silly and we're already
    storing the post ID. Instead, route from url strings to post numbers
    in the show method
  content - optional text. Body for self post.
  user - references. -> User model.
  *title  - mandatory string - title to display in link.

  Later feature slices will add categories, voting, and comments.

  ## REWORK POST MODEL-LEVEL VARIABLES
    target => url
    content => body
    DELETE location

So, missing CRUD is edit/update for posts. I'm almost fine with that,
but leaving the less exciting tasks for later means I wind up with a
gigantic pile of unpleasant tasks. Don't do that, it's silly.

Next up is sign in and sign up. This is going to be a bit weird since
I am doing things weirdly.