I'll want to use one or more of:
  https://github.com/jamescook/RubyRedditAPI
  https://github.com/jnunemaker/httparty

  Yup, definitely want to use HTTParty at a MINIMUM for interacting
  with reddit's API.

  I think what I want to do is write my front-end as API-consuming
  javascript, and structure my back-end's API to neatly cover reddit's API

  That way, I can toggle between consuming a subset of reddit's features
  and the features from my server. There might be some things, like
  asking about whether something needs a captcha and displaying one,
  that will be needed to consume reddit's API but not mine.

  I'll need to be rather explicit about naming, conventions, and
  building things in a way that avoids hard-coding things and
  can get hot-swapped to consuming a similar API.

  I want to start by consuming reddit's API and writing the
  front-end, and then going back and making my API work with it. It'll
  let me seamlessly add a 'browse reddit' mode, and give a custom reddit
  client.


  From a technology perspective, it's going to be rails backend serving
  an API with a subset of reddit's API routes, and a backbone front-end
  using that subset of reddit's API routes.

  Design is going to be a single-page experience, with the #-using
  URL partials. I'll expand out the browsing strategy from simply
  "next page in subreddit" to something configurable. I'll want a method
  that figures out how to get more links - it'll re-implement the
  "figure out how to ask for the next page", but it can do fancy stuff
  like "query the project API for suggestions" or "get the next page from
  all and remove everything that is SFW".

  I think I'll want an expanded-view only. That is, load/preload the:
    Title
    URL
      and render the image if it's an image
      and render the webpage if it's a webpage
    Top N comments
      give buttons for expanding them out
    Next Post (option to open in new tab?)
    Previous Post

  My thinking on this is that I might as well make a fairly clean break
  with the reddit-style presentation. It's pretty ugly, and pages full of
  expandable links is a headache to manage. Keep it simple.